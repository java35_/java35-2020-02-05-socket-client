package v1;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

public class ClientApp {

	static String SERVER_HOST = "127.0.0.1";
	static int SERVER_PORT = 5000;
	
	public static void main(String[] args) throws Exception {
		System.out.println("Client started...");
		Socket clientSocket = new Socket(SERVER_HOST, SERVER_PORT);
		
		InputStream is = clientSocket.getInputStream();
		OutputStream os = clientSocket.getOutputStream();
		
		ObjectInputStream ois = new ObjectInputStream(is);
		ObjectOutputStream oos = new ObjectOutputStream(os);
		
		oos.writeObject("Hello");
		oos.flush();
		System.out.println("Sent: " + "Hello");
		
		String incomingMessage = (String)ois.readObject();
		System.out.println("Recieved: " + incomingMessage);
		
		System.out.println("Clint off");
	}

}
