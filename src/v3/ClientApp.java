package v3;

public class ClientApp {

	static String SERVER_HOST = "127.0.0.1";
	static int SERVER_PORT = 5000;
	
	public static void main(String[] args) throws Exception {
		Client client = new Client(SERVER_HOST, SERVER_PORT);
		client.send();
		client.close();
		
	}

}
