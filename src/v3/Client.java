package v3;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

import v3.dto.Car;

public class Client implements Closeable {
	
	String serverHost;
	int serverPort;
	Socket clientSocket;
	
	public Client(String serverHost, int serverPort) {
		this.serverHost = serverHost;
		this.serverPort = serverPort;
	}
	
	public void send() throws Exception {
		System.out.println("Client started...");
		clientSocket = new Socket(serverHost, serverPort);
		
		InputStream is = clientSocket.getInputStream();
		OutputStream os = clientSocket.getOutputStream();
		
		ObjectInputStream ois = new ObjectInputStream(is);
		ObjectOutputStream oos = new ObjectOutputStream(os);
		
		Car car = new Car(2, "model2");
		oos.writeObject(car);
		oos.flush();
		System.out.println("Sent: " + car);
		
		Car incomingCar = (Car)ois.readObject();
		System.out.println("Recieved: " + incomingCar);
		
		System.out.println("Clint off");
	}

	@Override
	public void close() throws IOException {
		clientSocket.close();
		
	}
}
