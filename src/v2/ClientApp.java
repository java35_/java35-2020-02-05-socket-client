package v2;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

import v2.dto.Car;

public class ClientApp {

	static String SERVER_HOST = "127.0.0.1";
	static int SERVER_PORT = 5000;
	
	public static void main(String[] args) throws Exception {
		System.out.println("Client started...");
		Socket clientSocket = new Socket(SERVER_HOST, SERVER_PORT);
		
		InputStream is = clientSocket.getInputStream();
		OutputStream os = clientSocket.getOutputStream();
		
		ObjectInputStream ois = new ObjectInputStream(is);
		ObjectOutputStream oos = new ObjectOutputStream(os);
		
		Car car = new Car(2, "model2");
		oos.writeObject(car);
		oos.flush();
		System.out.println("Sent: " + car);
		
		Car incomingCar = (Car)ois.readObject();
		System.out.println("Recieved: " + incomingCar);
		
		System.out.println("Clint off");
	}

}
